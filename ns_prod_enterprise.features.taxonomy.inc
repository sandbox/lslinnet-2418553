<?php
/**
 * @file
 * ns_prod_enterprise.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function ns_prod_enterprise_taxonomy_default_vocabularies() {
  return array(
    'ns_prod_enterprise_syndication' => array(
      'name' => 'Syndication',
      'machine_name' => 'ns_prod_enterprise_syndication',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'ns_prod_enterprise_tag' => array(
      'name' => 'Tags',
      'machine_name' => 'ns_prod_enterprise_tag',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
