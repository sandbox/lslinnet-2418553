<?php
/**
 * @file
 * ns_prod_enterprise.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function ns_prod_enterprise_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ns_prod_entrpr_attachments|node|ns_page|form';
  $field_group->group_name = 'group_ns_prod_entrpr_attachments';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ns_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Attachments',
    'weight' => '4',
    'children' => array(
      0 => 'field_ns_fact_ns_page_fact',
      1 => 'field_ns_page_link_list',
      2 => 'field_ns_page_attach_files',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_ns_prod_entrpr_attachments|node|ns_page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ns_prod_entrpr_promo|node|ns_page|form';
  $field_group->group_name = 'group_ns_prod_entrpr_promo';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ns_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Promos',
    'weight' => '5',
    'children' => array(
      0 => 'field_ns_prod_entrpr_promo',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_ns_prod_entrpr_promo|node|ns_page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ns_prod_entrpr_promo|node|ns_prod_enterprise_section|form';
  $field_group->group_name = 'group_ns_prod_entrpr_promo';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ns_prod_enterprise_section';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Promos',
    'weight' => '5',
    'children' => array(
      0 => 'field_ns_prod_entrpr_form',
      1 => 'field_ns_prod_entrpr_promo',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_ns_prod_entrpr_promo|node|ns_prod_enterprise_section|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ns_prod_entrpr_slideshow|node|ns_prod_enterprise_section|form';
  $field_group->group_name = 'group_ns_prod_entrpr_slideshow';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ns_prod_enterprise_section';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Slideshow',
    'weight' => '3',
    'children' => array(
      0 => 'field_ns_prod_entrpr_slideshow',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_ns_prod_entrpr_slideshow|node|ns_prod_enterprise_section|form'] = $field_group;

  return $export;
}
