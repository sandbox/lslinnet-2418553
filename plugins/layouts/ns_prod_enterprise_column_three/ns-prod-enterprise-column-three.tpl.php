<?php

/**
 * @file
 * This layout is intended to be used inside the page content pane. Thats why
 * there is not wrapper div by default.
 */
?>

<?php if (!empty($content['header'])): ?>
  <div class="header">
      <?php print render($content['header']); ?>
  </div>
<?php endif; ?>

<?php if (!empty($content['col1'])): ?>
  <div class="col-1 col first">
    <?php print render($content['col1']); ?>
  </div>
<?php endif; ?>
<?php if (!empty($content['col2'])): ?>
  <div class="col-2 col">
    <?php print render($content['col2']); ?>
  </div>
<?php endif; ?>

<?php if (!empty($content['col3'])): ?>
  <div class="col-3 col last">
    <?php print render($content['col3']); ?>
  </div>
<?php endif; ?>
<?php if (!empty($content['footer'])): ?>
  <div class="footer">
      <?php print render($content['footer']); ?>
  </div>
<?php endif; ?>
