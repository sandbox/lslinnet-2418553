<?php

$plugin = array(
  'title' => t('Three columns'),
  'theme' => 'ns_prod_enterprise_column_three',
  'icon' => 'ns-prod-enterprise-column-three.png',
  'category' => 'NodeStream Enterprise',
  'regions' => array(
    'header' => t('Header'),
    'col1' => t('First column'),
    'col2' => t('Second column'),
    'col3' => t('Third column'),
    'footer' => t('Footer'),
  ),
);

/**
 * Implementation of theme_preprocess_ns_prod_enterprise_column_three().
 */
function ns_prod_enterprise_preprocess_ns_prod_enterprise_column_three(&$vars) {
  ns_core_check_layout_variables($vars);
}

