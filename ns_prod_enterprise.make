api = 2

core = 7.x

projects[adaptive_image][version] = 1.4
projects[adaptive_image][subdir] = contrib
projects[addressfield][version] = 1.0
projects[addressfield][subdir] = contrib
projects[context_admin][version] = 1.2
projects[context_admin][subdir] = contrib
projects[fences][version] = 1.0
projects[fences][subdir] = contrib
projects[fape][version] = 1.2
projects[fape][subdir] = contrib
projects[smartcrop][version] = 1.0-beta2
projects[smartcrop][subdir] = contrib
projects[media_vimeo][version] = 2.0
projects[media_vimeo][subdir] = contrib
projects[search_api][version] = 1.14
projects[search_api][subdir] = contrib
projects[search_api_db][version] = 1.4
projects[search_api_db][subdir] = contrib
projects[facetapi][version] = 1.5
projects[facetapi][subdir] = contrib
projects[views_bulk_operations][version] = 3.2
projects[views_bulk_operations][subdir] = contrib
projects[geofield][version] = 1.2
projects[geofield][subdir] = contrib
projects[geophp][version] = 1.7
projects[geophp][subdir] = contrib
projects[openlayers][version] = 2.0-beta11
projects[openlayers][subdir] = contrib
projects[geocoder][version] = 1.2
projects[geocoder][subdir] = contrib
projects[semantic_panels][version] = 1.2
projects[semantic_panels][subdir] = contrib
projects[content_menu][version] = 1.0
projects[content_menu][subdir] = contrib

; Themes
projects[lucid][type] = theme
projects[lucid][version] = 1.0-alpha2
