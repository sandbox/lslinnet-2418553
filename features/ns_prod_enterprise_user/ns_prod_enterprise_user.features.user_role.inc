<?php
/**
 * @file
 * ns_prod_enterprise_user.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function ns_prod_enterprise_user_user_default_roles() {
  $roles = array();

  // Exported role: employee.
  $roles['employee'] = array(
    'name' => 'employee',
    'weight' => '12',
  );

  return $roles;
}
