<?php
/**
 * @file
 * ns_prod_enterprise_user.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function ns_prod_enterprise_user_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ns_prod_entrpr_account|user|user|form';
  $field_group->group_name = 'group_ns_prod_entrpr_account';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Account information',
    'weight' => '0',
    'children' => array(
      0 => 'account',
      1 => 'timezone',
      2 => 'locale',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Account information',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_ns_prod_entrpr_account|user|user|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ns_prod_entrpr_personal|user|user|form';
  $field_group->group_name = 'group_ns_prod_entrpr_personal';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Personal information',
    'weight' => '2',
    'children' => array(
      0 => 'field_ns_prod_entrpr_body',
      1 => 'field_ns_prod_entrpr_email',
      2 => 'field_ns_prod_entrpr_lead',
      3 => 'field_ns_prod_entrpr_media',
      4 => 'field_ns_prod_entrpr_phone',
      5 => 'field_ns_prod_entrpr_position',
      6 => 'field_ns_prod_entrpr_office',
      7 => 'field_ns_prod_entrpr_link',
      8 => 'field_ns_prod_entrpr_first_name',
      9 => 'field_ns_prod_entrpr_last_name',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_ns_prod_entrpr_personal|user|user|form'] = $field_group;

  return $export;
}
