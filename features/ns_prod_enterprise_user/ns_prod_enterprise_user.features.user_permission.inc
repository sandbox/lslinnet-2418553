<?php
/**
 * @file
 * ns_prod_enterprise_user.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ns_prod_enterprise_user_user_default_permissions() {
  $permissions = array();

  // Exported permission: access user profiles.
  $permissions['access user profiles'] = array(
    'name' => 'access user profiles',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
      2 => 'authenticated user',
      3 => 'super user',
    ),
    'module' => 'user',
  );

  return $permissions;
}
