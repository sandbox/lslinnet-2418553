<?php
/**
 * @file
 * ns_prod_enterprise_client.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ns_prod_enterprise_client_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function ns_prod_enterprise_client_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function ns_prod_enterprise_client_node_info() {
  $items = array(
    'ns_prod_enterprise_case' => array(
      'name' => t('Business case'),
      'base' => 'node_content',
      'description' => t('Describe a business case that has been done in the organisation.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'ns_prod_enterprise_client' => array(
      'name' => t('Client'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
