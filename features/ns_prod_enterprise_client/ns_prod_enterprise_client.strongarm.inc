<?php
/**
 * @file
 * ns_prod_enterprise_client.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function ns_prod_enterprise_client_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_ns_prod_enterprise_case';
  $strongarm->value = 0;
  $export['comment_anonymous_ns_prod_enterprise_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_ns_prod_enterprise_case';
  $strongarm->value = 1;
  $export['comment_default_mode_ns_prod_enterprise_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_ns_prod_enterprise_case';
  $strongarm->value = '50';
  $export['comment_default_per_page_ns_prod_enterprise_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_ns_prod_enterprise_case';
  $strongarm->value = 1;
  $export['comment_form_location_ns_prod_enterprise_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_ns_prod_enterprise_case';
  $strongarm->value = '2';
  $export['comment_ns_prod_enterprise_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_ns_prod_enterprise_case';
  $strongarm->value = '1';
  $export['comment_preview_ns_prod_enterprise_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_ns_prod_enterprise_case';
  $strongarm->value = 1;
  $export['comment_subject_field_ns_prod_enterprise_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__ns_prod_enterprise_case';
  $strongarm->value = array(
    'view_modes' => array(),
    'extra_fields' => array(
      'form' => array(
        'path' => array(
          'weight' => '8',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__ns_prod_enterprise_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_ns_prod_enterprise_case';
  $strongarm->value = array();
  $export['menu_options_ns_prod_enterprise_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_ns_prod_enterprise_client';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_ns_prod_enterprise_client'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_ns_prod_enterprise_case';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_ns_prod_enterprise_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_ns_prod_enterprise_client';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_ns_prod_enterprise_client'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_ns_prod_enterprise_case';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );
  $export['node_options_ns_prod_enterprise_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_ns_prod_enterprise_client';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );
  $export['node_options_ns_prod_enterprise_client'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_ns_prod_enterprise_case';
  $strongarm->value = '0';
  $export['node_preview_ns_prod_enterprise_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_ns_prod_enterprise_client';
  $strongarm->value = '0';
  $export['node_preview_ns_prod_enterprise_client'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_ns_prod_enterprise_case';
  $strongarm->value = 1;
  $export['node_submitted_ns_prod_enterprise_case'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_ns_prod_enterprise_client';
  $strongarm->value = 0;
  $export['node_submitted_ns_prod_enterprise_client'] = $strongarm;

  return $export;
}
