<?php
/**
 * @file
 * ns_prod_enterprise_office.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ns_prod_enterprise_office_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'ns_prod_enterprise_office';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'taxonomy_term_data';
  $view->human_name = 'Offices';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Taxonomy term: Entity translation: translations */
  $handler->display->display_options['relationships']['entity_translations']['id'] = 'entity_translations';
  $handler->display->display_options['relationships']['entity_translations']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['relationships']['entity_translations']['field'] = 'entity_translations';
  $handler->display->display_options['relationships']['entity_translations']['required'] = 1;
  /* Field: Taxonomy term: Entity label */
  $handler->display->display_options['fields']['label']['id'] = 'label';
  $handler->display->display_options['fields']['label']['table'] = 'views_entity_taxonomy_term';
  $handler->display->display_options['fields']['label']['field'] = 'label';
  $handler->display->display_options['fields']['label']['label'] = '';
  $handler->display->display_options['fields']['label']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['label']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['label']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['label']['alter']['external'] = 0;
  $handler->display->display_options['fields']['label']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['label']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['label']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['label']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['label']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['label']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['label']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['label']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['label']['alter']['html'] = 0;
  $handler->display->display_options['fields']['label']['element_type'] = 'h3';
  $handler->display->display_options['fields']['label']['element_class'] = 'title';
  $handler->display->display_options['fields']['label']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['label']['element_default_classes'] = 0;
  $handler->display->display_options['fields']['label']['hide_empty'] = 0;
  $handler->display->display_options['fields']['label']['empty_zero'] = 0;
  $handler->display->display_options['fields']['label']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['label']['link_to_entity'] = 1;
  /* Field: Taxonomy term: Address */
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['id'] = 'field_ns_prod_entrpr_address';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['table'] = 'field_data_field_ns_prod_entrpr_address';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['field'] = 'field_ns_prod_entrpr_address';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['label'] = '';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['element_type'] = '0';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['element_default_classes'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['click_sort_column'] = 'country';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['settings'] = array(
    'use_widget_handlers' => 1,
    'format_handlers' => array(
      'address' => 'address',
    ),
  );
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['field_api_classes'] = 1;
  /* Field: Field: Phone number */
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['id'] = 'field_ns_prod_entrpr_phone';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['table'] = 'field_data_field_ns_prod_entrpr_phone';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['field'] = 'field_ns_prod_entrpr_phone';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['label'] = '';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['element_type'] = '0';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['element_default_classes'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['group_rows'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['delta_limit'] = '1';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['delta_reversed'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['delta_first_last'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_phone']['field_api_classes'] = 1;
  /* Field: Field: Email */
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['id'] = 'field_ns_prod_entrpr_email';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['table'] = 'field_data_field_ns_prod_entrpr_email';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['field'] = 'field_ns_prod_entrpr_email';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['label'] = '';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['element_type'] = '0';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['element_default_classes'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_email']['field_api_classes'] = 1;
  /* Filter criterion: Taxonomy vocabulary: Machine name */
  $handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
  $handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['value'] = array(
    'ns_prod_enterprise_office' => 'ns_prod_enterprise_office',
  );
  /* Filter criterion: Entity translation: Language */
  $handler->display->display_options['filters']['language']['id'] = 'language';
  $handler->display->display_options['filters']['language']['table'] = 'entity_translation';
  $handler->display->display_options['filters']['language']['field'] = 'language';
  $handler->display->display_options['filters']['language']['relationship'] = 'entity_translations';
  $handler->display->display_options['filters']['language']['value'] = array(
    '***CURRENT_LANGUAGE***' => '***CURRENT_LANGUAGE***',
    'und' => 'und',
  );
  $handler->display->display_options['filters']['language']['expose']['operator_id'] = 'language_op';
  $handler->display->display_options['filters']['language']['expose']['label'] = 'Language';
  $handler->display->display_options['filters']['language']['expose']['operator'] = 'language_op';
  $handler->display->display_options['filters']['language']['expose']['identifier'] = 'language';
  $handler->display->display_options['filters']['language']['expose']['reduce'] = 0;

  /* Display: Listing */
  $handler = $view->new_display('panel_pane', 'Listing', 'panel_pane_1');
  $handler->display->display_options['pane_category']['name'] = 'Content';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 'items_per_page';
  $handler->display->display_options['allow']['offset'] = 'offset';
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 'title_override';
  $handler->display->display_options['allow']['exposed_form'] = 'exposed_form';
  $handler->display->display_options['allow']['fields_override'] = 'fields_override';

  /* Display: Grid */
  $handler = $view->new_display('panel_pane', 'Grid', 'panel_pane_2');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'grid';
  $handler->display->display_options['style_options']['fill_single_line'] = 1;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['pane_category']['name'] = 'Content';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 'items_per_page';
  $handler->display->display_options['allow']['offset'] = 'offset';
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 'title_override';
  $handler->display->display_options['allow']['exposed_form'] = 'exposed_form';
  $handler->display->display_options['allow']['fields_override'] = 'fields_override';

  /* Display: List */
  $handler = $view->new_display('panel_pane', 'List', 'panel_pane_3');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Taxonomy term: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['name']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['name']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['name']['alter']['external'] = 0;
  $handler->display->display_options['fields']['name']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['name']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['name']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['name']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['name']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['name']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['name']['alter']['html'] = 0;
  $handler->display->display_options['fields']['name']['element_type'] = '0';
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['name']['element_default_classes'] = 0;
  $handler->display->display_options['fields']['name']['hide_empty'] = 0;
  $handler->display->display_options['fields']['name']['empty_zero'] = 0;
  $handler->display->display_options['fields']['name']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['name']['link_to_taxonomy'] = 1;
  $handler->display->display_options['fields']['name']['convert_spaces'] = 0;
  $handler->display->display_options['pane_category']['name'] = 'Content';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $handler->display->display_options['allow']['use_pager'] = 'use_pager';
  $handler->display->display_options['allow']['items_per_page'] = 'items_per_page';
  $handler->display->display_options['allow']['offset'] = 'offset';
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 'title_override';
  $handler->display->display_options['allow']['exposed_form'] = 'exposed_form';
  $handler->display->display_options['allow']['fields_override'] = 0;
  $translatables['ns_prod_enterprise_office'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Translations'),
    t('Language'),
    t('Listing'),
    t('Content'),
    t('Grid'),
    t('List'),
  );
  $export['ns_prod_enterprise_office'] = $view;

  return $export;
}
