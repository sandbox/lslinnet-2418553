<?php
/**
 * @file
 * ns_prod_enterprise_office_map.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ns_prod_enterprise_office_map_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'ns_prod_enterprise_office_map';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'taxonomy_term_data';
  $view->human_name = 'Map';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'openlayers_data';
  $handler->display->display_options['style_options']['data_source'] = array(
    'value' => 'wkt',
    'other_lat' => 'label',
    'other_lon' => 'field_ns_prod_entrpr_location',
    'wkt' => 'field_ns_prod_entrpr_location',
    'other_top' => 'label',
    'other_right' => 'label',
    'other_bottom' => 'label',
    'other_left' => 'label',
    'name_field' => 'label',
    'description_field' => 'field_ns_prod_entrpr_address',
  );
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Taxonomy term: Entity label */
  $handler->display->display_options['fields']['label']['id'] = 'label';
  $handler->display->display_options['fields']['label']['table'] = 'views_entity_taxonomy_term';
  $handler->display->display_options['fields']['label']['field'] = 'label';
  /* Field: Taxonomy term: Location */
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['id'] = 'field_ns_prod_entrpr_location';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['table'] = 'field_data_field_ns_prod_entrpr_location';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['field'] = 'field_ns_prod_entrpr_location';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['label'] = '';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['element_type'] = '0';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['element_default_classes'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['click_sort_column'] = 'wkt';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['settings'] = array(
    'data' => 'full',
  );
  $handler->display->display_options['fields']['field_ns_prod_entrpr_location']['field_api_classes'] = 0;
  /* Field: Taxonomy term: Address */
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['id'] = 'field_ns_prod_entrpr_address';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['table'] = 'field_data_field_ns_prod_entrpr_address';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['field'] = 'field_ns_prod_entrpr_address';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['label'] = '';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['element_type'] = '0';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['element_default_classes'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['click_sort_column'] = 'country';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['settings'] = array(
    'use_widget_handlers' => 1,
    'format_handlers' => array(
      'address' => 'address',
    ),
  );
  $handler->display->display_options['fields']['field_ns_prod_entrpr_address']['field_api_classes'] = 1;

  /* Display: OpenLayers Data Overlay */
  $handler = $view->new_display('openlayers', 'OpenLayers Data Overlay', 'openlayers_1');
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Taxonomy term: Location (field_ns_prod_entrpr_location:wkt) */
  $handler->display->display_options['filters']['field_ns_prod_entrpr_location_wkt']['id'] = 'field_ns_prod_entrpr_location_wkt';
  $handler->display->display_options['filters']['field_ns_prod_entrpr_location_wkt']['table'] = 'field_data_field_ns_prod_entrpr_location';
  $handler->display->display_options['filters']['field_ns_prod_entrpr_location_wkt']['field'] = 'field_ns_prod_entrpr_location_wkt';
  $handler->display->display_options['filters']['field_ns_prod_entrpr_location_wkt']['operator'] = 'not empty';
  $translatables['ns_prod_enterprise_office_map'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('Entity label'),
    t('OpenLayers Data Overlay'),
  );
  $export['ns_prod_enterprise_office_map'] = $view;

  return $export;
}
