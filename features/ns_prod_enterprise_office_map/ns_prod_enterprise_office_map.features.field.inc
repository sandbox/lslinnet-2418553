<?php
/**
 * @file
 * ns_prod_enterprise_office_map.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function ns_prod_enterprise_office_map_field_default_fields() {
  $fields = array();

  // Exported field: 'taxonomy_term-ns_prod_enterprise_office-field_ns_prod_entrpr_location'.
  $fields['taxonomy_term-ns_prod_enterprise_office-field_ns_prod_entrpr_location'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_ns_prod_entrpr_location',
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => '0',
      'module' => 'geofield',
      'settings' => array(),
      'translatable' => '0',
      'type' => 'geofield',
    ),
    'field_instance' => array(
      'bundle' => 'ns_prod_enterprise_office',
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'geofield',
          'settings' => array(
            'data' => 'full',
          ),
          'type' => 'geofield_wkt',
          'weight' => 9,
        ),
      ),
      'entity_type' => 'taxonomy_term',
      'fences_wrapper' => 'div',
      'field_name' => 'field_ns_prod_entrpr_location',
      'label' => 'Location',
      'required' => 0,
      'settings' => array(
        'local_solr' => array(
          'enabled' => FALSE,
          'lat_field' => 'lat',
          'lng_field' => 'lng',
        ),
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'geocoder',
        'settings' => array(
          'delta_handling' => 'default',
          'geocoder_field' => 'field_ns_prod_entrpr_address',
          'geocoder_handler' => 'google',
          'handler_settings' => array(
            'google' => array(
              'all_results' => 0,
              'geometry_type' => 'point',
              'reject_results' => array(
                'APPROXIMATE' => 0,
                'GEOMETRIC_CENTER' => 0,
                'RANGE_INTERPOLATED' => 0,
                'ROOFTOP' => 0,
              ),
            ),
          ),
        ),
        'type' => 'geocoder',
        'weight' => '7',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');

  return $fields;
}
