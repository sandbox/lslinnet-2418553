<?php
/**
 * @file
 * ns_prod_enterprise_service.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ns_prod_enterprise_service_user_default_permissions() {
  $permissions = array();

  // Exported permission: create ns_prod_enterprise_service content.
  $permissions['create ns_prod_enterprise_service content'] = array(
    'name' => 'create ns_prod_enterprise_service content',
    'roles' => array(
      0 => 'administrator',
      1 => 'chief editor',
      2 => 'editor',
      3 => 'super user',
      4 => 'writer',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any ns_prod_enterprise_service content.
  $permissions['delete any ns_prod_enterprise_service content'] = array(
    'name' => 'delete any ns_prod_enterprise_service content',
    'roles' => array(
      0 => 'administrator',
      1 => 'chief editor',
      2 => 'editor',
      3 => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own ns_prod_enterprise_service content.
  $permissions['delete own ns_prod_enterprise_service content'] = array(
    'name' => 'delete own ns_prod_enterprise_service content',
    'roles' => array(
      0 => 'administrator',
      1 => 'chief editor',
      2 => 'editor',
      3 => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any ns_prod_enterprise_service content.
  $permissions['edit any ns_prod_enterprise_service content'] = array(
    'name' => 'edit any ns_prod_enterprise_service content',
    'roles' => array(
      0 => 'administrator',
      1 => 'chief editor',
      2 => 'editor',
      3 => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own ns_prod_enterprise_service content.
  $permissions['edit own ns_prod_enterprise_service content'] = array(
    'name' => 'edit own ns_prod_enterprise_service content',
    'roles' => array(
      0 => 'administrator',
      1 => 'chief editor',
      2 => 'editor',
      3 => 'super user',
      4 => 'writer',
    ),
    'module' => 'node',
  );

  return $permissions;
}
