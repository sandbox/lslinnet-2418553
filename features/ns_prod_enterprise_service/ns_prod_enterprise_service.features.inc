<?php
/**
 * @file
 * ns_prod_enterprise_service.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ns_prod_enterprise_service_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function ns_prod_enterprise_service_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function ns_prod_enterprise_service_node_info() {
  $items = array(
    'ns_prod_enterprise_service' => array(
      'name' => t('Service'),
      'base' => 'node_content',
      'description' => t('Describe a service that you provide.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
