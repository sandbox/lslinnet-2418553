<?php
/**
 * @file
 * ns_prod_enterprise_blog.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ns_prod_enterprise_blog_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "defaultconfig" && $api == "defaultconfig_optionals_settings") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "panels_mini" && $api == "panels_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function ns_prod_enterprise_blog_views_api() {
  return array("version" => "3.0");
}
