<?php
/**
 * @file
 * ns_prod_enterprise_blog.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function ns_prod_enterprise_blog_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ns_prod_entrpr_attach|node|ns_blog_post|form';
  $field_group->group_name = 'group_ns_prod_entrpr_attach';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ns_blog_post';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Attachments',
    'weight' => '5',
    'children' => array(
      0 => 'field_ns_blog_post_related',
      1 => 'field_ns_blog_post_file_attach',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_ns_prod_entrpr_attach|node|ns_blog_post|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ns_prod_entrpr_category|node|ns_blog_post|form';
  $field_group->group_name = 'group_ns_prod_entrpr_category';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ns_blog_post';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Categories',
    'weight' => '6',
    'children' => array(
      0 => 'field_ns_prod_entrpr_syndication',
      1 => 'field_ns_prod_entrpr_tag',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_ns_prod_entrpr_category|node|ns_blog_post|form'] = $field_group;

  return $export;
}
