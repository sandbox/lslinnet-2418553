<?php
/**
 * @file
 * ns_prod_enterprise.file_default_displays.inc
 */

/**
 * Implements hook_file_default_displays().
 */
function ns_prod_enterprise_file_default_displays() {
  $export = array();

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__ns_prod_entrprise_avatar__file_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'ns_prod_enterprise_avatar',
    'alt' => '[file:name]',
    'title' => '[file:field_ns_media_caption]',
  );
  $export['image__ns_prod_entrprise_avatar__file_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__ns_prod_entrprise_content__file_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'ns_prod_enterprise_content',
    'alt' => '[file:name]',
    'title' => '[file:field_ns_media_caption]',
  );
  $export['image__ns_prod_entrprise_content__file_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__ns_prod_entrprise_large__file_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'ns_prod_enterprise_large',
    'alt' => '[file:name]',
    'title' => '[file:field_ns_media_caption]',
  );
  $export['image__ns_prod_entrprise_large__file_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__ns_prod_entrprise_small__file_field_media_large_icon';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = '';
  $export['image__ns_prod_entrprise_small__file_field_media_large_icon'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__ns_prod_entrprise_small__file_field_panels_ref_ref_formatter_mini';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = array(
    'mini_panel' => '',
    'language_filter' => 1,
    'cols' => '0',
  );
  $export['image__ns_prod_entrprise_small__file_field_panels_ref_ref_formatter_mini'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__ns_prod_entrprise_small__file_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'ns_prod_enterprise_small',
    'alt' => '[file:name]',
    'title' => '[file:field_ns_media_caption]',
  );
  $export['image__ns_prod_entrprise_small__file_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__ns_prod_entrprise_thumb_3col__file_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'ns_prod_enterprise_thumb_3col',
    'alt' => '[file:field-ns-media-caption]',
    'title' => '[file:name]',
  );
  $export['image__ns_prod_entrprise_thumb_3col__file_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__ns_prod_entrprise_thumb__file_field_media_large_icon';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = '';
  $export['image__ns_prod_entrprise_thumb__file_field_media_large_icon'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__ns_prod_entrprise_thumb__file_field_panels_ref_ref_formatter_mini';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = array(
    'mini_panel' => '',
    'language_filter' => 1,
    'cols' => '0',
  );
  $export['image__ns_prod_entrprise_thumb__file_field_panels_ref_ref_formatter_mini'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__ns_prod_entrprise_thumb__file_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'ns_prod_enterprise_thumbnail',
    'alt' => '[file:name]',
    'title' => '[file:field_ns_media_caption]',
  );
  $export['image__ns_prod_entrprise_thumb__file_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__ns_prod_entrprise_thumb_small__file_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'ns_prod_enterprise_thumb_small',
    'alt' => '[file:name]',
    'title' => '[file:field-ns-media-caption]',
  );
  $export['image__ns_prod_entrprise_thumb_small__file_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__ns_prod_entrprise_xlarge__file_field_media_large_icon';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = '';
  $export['image__ns_prod_entrprise_xlarge__file_field_media_large_icon'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__ns_prod_entrprise_xlarge__file_field_panels_ref_ref_formatter_mini';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = array(
    'mini_panel' => '',
    'language_filter' => 1,
    'cols' => '0',
  );
  $export['image__ns_prod_entrprise_xlarge__file_field_panels_ref_ref_formatter_mini'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'image__ns_prod_entrprise_xlarge__file_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'ns_prod_enterprise_xlarge',
    'alt' => '[file:name]',
    'title' => '[file:field_ns_media_caption]',
  );
  $export['image__ns_prod_entrprise_xlarge__file_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__ns_prod_entrprise_content__file_image';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = array(
    'image_style' => '',
  );
  $export['video__ns_prod_entrprise_content__file_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__ns_prod_entrprise_content__media_youtube_image';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = array(
    'image_style' => '',
  );
  $export['video__ns_prod_entrprise_content__media_youtube_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__ns_prod_entrprise_content__media_youtube_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'version' => '3',
    'theme' => 'dark',
    'width' => '460',
    'height' => '259',
    'fullscreen' => 1,
    'autoplay' => 0,
    'related' => 1,
    'hd' => 0,
    'showsearch' => 1,
    'modestbranding' => 0,
    'showinfo' => 1,
    'chromeless' => 0,
  );
  $export['video__ns_prod_entrprise_content__media_youtube_video'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__ns_prod_entrprise_large__media_vimeo_image';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = array(
    'image_style' => '',
  );
  $export['video__ns_prod_entrprise_large__media_vimeo_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__ns_prod_entrprise_large__media_vimeo_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'width' => '700',
    'height' => '394',
    'autoplay' => 0,
  );
  $export['video__ns_prod_entrprise_large__media_vimeo_video'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__ns_prod_entrprise_large__media_youtube_image';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = array(
    'image_style' => '',
  );
  $export['video__ns_prod_entrprise_large__media_youtube_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__ns_prod_entrprise_large__media_youtube_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'version' => '3',
    'theme' => 'dark',
    'width' => '700',
    'height' => '394',
    'fullscreen' => 1,
    'autoplay' => 0,
    'related' => 1,
    'hd' => 0,
    'showsearch' => 1,
    'modestbranding' => 0,
    'showinfo' => 1,
    'chromeless' => 0,
  );
  $export['video__ns_prod_entrprise_large__media_youtube_video'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__ns_prod_entrprise_small__file_image';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = array(
    'image_style' => '',
  );
  $export['video__ns_prod_entrprise_small__file_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__ns_prod_entrprise_small__media_youtube_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'ns_prod_enterprise_small',
  );
  $export['video__ns_prod_entrprise_small__media_youtube_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__ns_prod_entrprise_small__media_youtube_video';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = array(
    'version' => '3',
    'theme' => 'dark',
    'width' => '460',
    'height' => '345',
    'fullscreen' => 1,
    'autoplay' => 0,
    'related' => 1,
    'hd' => 0,
    'showsearch' => 1,
    'modestbranding' => 0,
    'showinfo' => 1,
    'chromeless' => 0,
  );
  $export['video__ns_prod_entrprise_small__media_youtube_video'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__ns_prod_entrprise_thumb_3col__file_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => '',
    'alt' => '[file:field-ns-media-caption]',
    'title' => '[file:name]',
  );
  $export['video__ns_prod_entrprise_thumb_3col__file_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__ns_prod_entrprise_thumb_3col__media_vimeo_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'ns_prod_enterprise_thumb_3col',
  );
  $export['video__ns_prod_entrprise_thumb_3col__media_vimeo_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__ns_prod_entrprise_thumb_3col__media_youtube_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'ns_prod_enterprise_thumb_3col',
  );
  $export['video__ns_prod_entrprise_thumb_3col__media_youtube_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__ns_prod_entrprise_thumb__file_image';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = array(
    'image_style' => '',
    'alt' => '[file:name]',
    'title' => '[file:name]',
  );
  $export['video__ns_prod_entrprise_thumb__file_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__ns_prod_entrprise_thumb__media_youtube_image';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'image_style' => 'ns_prod_enterprise_thumbnail',
  );
  $export['video__ns_prod_entrprise_thumb__media_youtube_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__ns_prod_entrprise_thumb__media_youtube_video';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = array(
    'version' => '3',
    'theme' => 'dark',
    'width' => '560',
    'height' => '340',
    'fullscreen' => 1,
    'autoplay' => 0,
    'related' => 1,
    'hd' => 0,
    'showsearch' => 1,
    'modestbranding' => 0,
    'showinfo' => 1,
    'chromeless' => 0,
  );
  $export['video__ns_prod_entrprise_thumb__media_youtube_video'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__ns_prod_entrprise_xlarge__file_image';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = array(
    'image_style' => '',
  );
  $export['video__ns_prod_entrprise_xlarge__file_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__ns_prod_entrprise_xlarge__media_youtube_image';
  $file_display->weight = 0;
  $file_display->status = FALSE;
  $file_display->settings = array(
    'image_style' => '',
  );
  $export['video__ns_prod_entrprise_xlarge__media_youtube_image'] = $file_display;

  $file_display = new stdClass();
  $file_display->api_version = 1;
  $file_display->name = 'video__ns_prod_entrprise_xlarge__media_youtube_video';
  $file_display->weight = 0;
  $file_display->status = TRUE;
  $file_display->settings = array(
    'version' => '3',
    'theme' => 'dark',
    'width' => '960',
    'height' => '540',
    'fullscreen' => 1,
    'autoplay' => 0,
    'related' => 1,
    'hd' => 0,
    'showsearch' => 1,
    'modestbranding' => 0,
    'showinfo' => 1,
    'chromeless' => 0,
  );
  $export['video__ns_prod_entrprise_xlarge__media_youtube_video'] = $file_display;

  return $export;
}
