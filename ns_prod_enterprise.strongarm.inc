<?php
/**
 * @file
 * ns_prod_enterprise.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function ns_prod_enterprise_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_ns_prod_enterprise_section';
  $strongarm->value = 0;
  $export['comment_anonymous_ns_prod_enterprise_section'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_ns_prod_enterprise_section';
  $strongarm->value = 1;
  $export['comment_default_mode_ns_prod_enterprise_section'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_ns_prod_enterprise_section';
  $strongarm->value = '50';
  $export['comment_default_per_page_ns_prod_enterprise_section'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_ns_prod_enterprise_section';
  $strongarm->value = 1;
  $export['comment_form_location_ns_prod_enterprise_section'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_ns_prod_enterprise_section';
  $strongarm->value = '2';
  $export['comment_ns_prod_enterprise_section'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_ns_prod_enterprise_section';
  $strongarm->value = '1';
  $export['comment_preview_ns_prod_enterprise_section'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_ns_prod_enterprise_section';
  $strongarm->value = 1;
  $export['comment_subject_field_ns_prod_enterprise_section'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_ns_prod_enterprise_section';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_ns_prod_enterprise_section'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_ns_prod_enterprise_section';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_ns_prod_enterprise_section'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_ns_prod_enterprise_section';
  $strongarm->value = array(
    0 => 'moderation',
    1 => 'revision',
  );
  $export['node_options_ns_prod_enterprise_section'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_ns_prod_enterprise_section';
  $strongarm->value = '1';
  $export['node_preview_ns_prod_enterprise_section'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_ns_prod_enterprise_section';
  $strongarm->value = 0;
  $export['node_submitted_ns_prod_enterprise_section'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'page_manager_node_view_disabled';
  $strongarm->value = 0;
  $export['page_manager_node_view_disabled'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_defaults_node_ns_prod_enterprise_section';
  $strongarm->value = array(
    'status' => 1,
    'default' => 1,
    'choice' => 0,
  );
  $export['panelizer_defaults_node_ns_prod_enterprise_section'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_node:ns_prod_enterprise_section_allowed_layouts';
  $strongarm->value = 'O:22:"panels_allowed_layouts":4:{s:9:"allow_new";b:1;s:11:"module_name";s:41:"panelizer_node:ns_prod_enterprise_section";s:23:"allowed_layout_settings";a:18:{s:8:"flexible";b:0;s:21:"ns_core_site_template";b:0;s:13:"ns_core_naked";b:0;s:26:"ns-core-content-column-two";b:0;s:20:"ns_core_column_three";b:1;s:18:"ns_core_column_two";b:0;s:26:"ns-core-content-column-one";b:0;s:18:"ns_core_column_one";b:1;s:14:"twocol_stacked";b:0;s:17:"threecol_33_34_33";b:0;s:6:"onecol";b:1;s:6:"twocol";b:1;s:17:"threecol_25_50_25";b:0;s:25:"threecol_25_50_25_stacked";b:0;s:13:"twocol_bricks";b:0;s:25:"threecol_33_34_33_stacked";b:0;s:13:"site_template";b:0;s:31:"ns_prod_enterprise_column_three";b:1;}s:10:"form_state";N;}';
  $export['panelizer_node:ns_prod_enterprise_section_allowed_layouts'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_node:ns_prod_enterprise_section_allowed_types';
  $strongarm->value = array(
    'context_admin-context_admin:user_create_menu' => 0,
    'context_admin-context_admin:taxonomy_list_menu' => 0,
    'context_admin-context_admin:term_options_menu' => 0,
    'context_admin-context_admin:node_edit_menu' => 0,
    'context_admin-context_admin:node_create_menu' => 0,
    'context_admin-context_admin:admin_section' => 0,
    'context_admin-context_admin:node_view_menu' => 0,
    'custom-custom' => 'custom-custom',
    'token-node:source' => 0,
    'token-node:log' => 0,
    'token-node:content-type' => 0,
    'token-node:nid' => 0,
    'token-node:vid' => 0,
    'token-node:title' => 0,
    'token-node:body' => 0,
    'token-node:summary' => 0,
    'token-node:language' => 0,
    'token-node:url' => 0,
    'token-node:edit-url' => 0,
    'token-node:created' => 0,
    'token-node:changed' => 0,
    'token-node:author' => 0,
    'token-node:menu-link' => 0,
    'token-node:is-new' => 0,
    'token-node:status' => 0,
    'token-node:promote' => 0,
    'token-node:sticky' => 0,
    'token-node:revision' => 0,
    'token-node:title-field' => 0,
    'token-node:field-ns-prod-entrpr-promo' => 0,
    'token-node:field-ns-page-body' => 0,
    'token-node:field-ns-page-media' => 0,
    'token-node:field-ns-page-lead' => 0,
    'token-node:field-ns-prod-entrpr-user' => 0,
    'token-node:field-ns-blog-description-long' => 0,
    'token-node:field-ns-blog-description-short' => 0,
    'token-node:field-ns-blog-image' => 0,
    'token-node:field-ns-prod-entrpr-syndication' => 0,
    'token-node:field-ns-prod-entrpr-tag' => 0,
    'token-node:field-ns-blog-post-blog' => 0,
    'token-node:field-ns-blog-post-body' => 0,
    'token-node:field-ns-blog-post-media' => 0,
    'token-node:field-ns-blog-post-lead' => 0,
    'token-node:field-ns-promo-description' => 0,
    'token-node:field-ns-promo-link' => 0,
    'token-node:field-ns-promo-media' => 0,
    'token-node:field-ns-prod-entrpr-body' => 0,
    'token-node:field-ns-prod-entrpr-lead' => 0,
    'token-node:field-ns-prod-entrpr-slideshow' => 0,
    'token-node:field-ns-prod-entrpr-media' => 0,
    'token-node:field-ns-prod-entrpr-case-study' => 0,
    'token-node:field-ns-prod-entrpr-client' => 0,
    'token-node:field-ns-prod-entrpr-url' => 0,
    'token-node:field-ns-prod-entrpr-fact' => 0,
    'token-node:title_field' => 0,
    'token-node:field_ns_prod_entrpr_body' => 0,
    'token-node:field_ns_prod_entrpr_lead' => 0,
    'token-node:field_ns_prod_entrpr_media' => 0,
    'token-node:field_ns_prod_entrpr_case_study' => 0,
    'token-node:field_ns_prod_entrpr_client' => 0,
    'token-node:field_ns_prod_entrpr_promo' => 0,
    'token-node:field_ns_prod_entrpr_url' => 0,
    'token-node:field_ns_prod_entrpr_user' => 0,
    'token-node:field_ns_prod_entrpr_syndication' => 0,
    'token-node:field_ns_prod_entrpr_tag' => 0,
    'token-node:field_ns_blog_post_blog' => 0,
    'token-node:field_ns_blog_description_long' => 0,
    'token-node:field_ns_blog_description_short' => 0,
    'token-node:field_ns_blog_image' => 0,
    'token-node:field_ns_blog_post_body' => 0,
    'token-node:field_ns_blog_post_media' => 0,
    'token-node:field_ns_blog_post_lead' => 0,
    'token-node:field_ns_page_body' => 0,
    'token-node:field_ns_page_media' => 0,
    'token-node:field_ns_page_lead' => 0,
    'token-node:field_ns_prod_entrpr_slideshow' => 0,
    'token-node:field_ns_promo_description' => 0,
    'token-node:field_ns_promo_link' => 0,
    'token-node:field_ns_promo_media' => 0,
    'token-node:field_ns_prod_entrpr_fact' => 0,
    'token-node:original' => 0,
    'token-content-type:name' => 0,
    'token-content-type:machine-name' => 0,
    'token-content-type:description' => 0,
    'token-content-type:node-count' => 0,
    'token-content-type:edit-url' => 0,
    'token-term:edit-url' => 0,
    'token-term:parents' => 0,
    'token-term:root' => 0,
    'token-term:tid' => 0,
    'token-term:name' => 0,
    'token-term:description' => 0,
    'token-term:node-count' => 0,
    'token-term:url' => 0,
    'token-term:vocabulary' => 0,
    'token-term:parent' => 0,
    'token-term:weight' => 0,
    'token-term:parents-all' => 0,
    'token-term:name-field' => 0,
    'token-term:description-field' => 0,
    'token-term:field-ns-prod-entrpr-media' => 0,
    'token-term:field-ns-prod-entrpr-body' => 0,
    'token-term:field-ns-prod-entrpr-email' => 0,
    'token-term:field-ns-prod-entrpr-phone' => 0,
    'token-term:field-ns-prod-entrpr-address' => 0,
    'token-term:field-ns-prod-entrpr-location' => 0,
    'token-term:name_field' => 0,
    'token-term:description_field' => 0,
    'token-term:field_ns_prod_entrpr_body' => 0,
    'token-term:field_ns_prod_entrpr_email' => 0,
    'token-term:field_ns_prod_entrpr_media' => 0,
    'token-term:field_ns_prod_entrpr_phone' => 0,
    'token-term:field_ns_prod_entrpr_address' => 0,
    'token-term:field_ns_prod_entrpr_location' => 0,
    'token-term:original' => 0,
    'token-vocabulary:machine-name' => 0,
    'token-vocabulary:edit-url' => 0,
    'token-vocabulary:vid' => 0,
    'token-vocabulary:name' => 0,
    'token-vocabulary:description' => 0,
    'token-vocabulary:node-count' => 0,
    'token-vocabulary:term-count' => 0,
    'token-vocabulary:original' => 0,
    'token-file:basename' => 0,
    'token-file:extension' => 0,
    'token-file:size-raw' => 0,
    'token-file:type' => 0,
    'token-file:fid' => 0,
    'token-file:name' => 0,
    'token-file:path' => 0,
    'token-file:mime' => 0,
    'token-file:size' => 0,
    'token-file:url' => 0,
    'token-file:timestamp' => 0,
    'token-file:owner' => 0,
    'token-file:field-ns-media-caption' => 0,
    'token-file:field-ns-media-category' => 0,
    'token-file:field-ns-media-tag' => 0,
    'token-file:field_ns_media_caption' => 0,
    'token-file:field_ns_media_category' => 0,
    'token-file:field_ns_media_tag' => 0,
    'token-file:original' => 0,
    'token-user:cancel-url' => 0,
    'token-user:one-time-login-url' => 0,
    'token-user:roles' => 0,
    'token-user:uid' => 0,
    'token-user:name' => 0,
    'token-user:mail' => 0,
    'token-user:url' => 0,
    'token-user:edit-url' => 0,
    'token-user:last-login' => 0,
    'token-user:created' => 0,
    'token-user:last-access' => 0,
    'token-user:status' => 0,
    'token-user:theme' => 0,
    'token-user:field-ns-prod-entrpr-body' => 0,
    'token-user:field-ns-prod-entrpr-email' => 0,
    'token-user:field-ns-prod-entrpr-first-name' => 0,
    'token-user:field-ns-prod-entrpr-last-name' => 0,
    'token-user:field-ns-prod-entrpr-lead' => 0,
    'token-user:field-ns-prod-entrpr-link' => 0,
    'token-user:field-ns-prod-entrpr-media' => 0,
    'token-user:field-ns-prod-entrpr-phone' => 0,
    'token-user:field-ns-prod-entrpr-position' => 0,
    'token-user:field-ns-prod-entrpr-office' => 0,
    'token-user:field_ns_prod_entrpr_body' => 0,
    'token-user:field_ns_prod_entrpr_email' => 0,
    'token-user:field_ns_prod_entrpr_first_name' => 0,
    'token-user:field_ns_prod_entrpr_last_name' => 0,
    'token-user:field_ns_prod_entrpr_lead' => 0,
    'token-user:field_ns_prod_entrpr_link' => 0,
    'token-user:field_ns_prod_entrpr_media' => 0,
    'token-user:field_ns_prod_entrpr_phone' => 0,
    'token-user:field_ns_prod_entrpr_position' => 0,
    'token-user:field_ns_prod_entrpr_office' => 0,
    'token-user:original' => 0,
    'token-current-user:ip-address' => 0,
    'token-menu-link:mlid' => 0,
    'token-menu-link:title' => 0,
    'token-menu-link:url' => 0,
    'token-menu-link:parent' => 0,
    'token-menu-link:parents' => 0,
    'token-menu-link:root' => 0,
    'token-menu-link:menu' => 0,
    'token-menu-link:edit-url' => 0,
    'token-current-page:title' => 0,
    'token-current-page:url' => 0,
    'token-current-page:page-number' => 0,
    'token-current-page:query' => 0,
    'token-url:path' => 0,
    'token-url:relative' => 0,
    'token-url:absolute' => 0,
    'token-url:brief' => 0,
    'token-url:unaliased' => 0,
    'token-url:args' => 0,
    'token-array:first' => 0,
    'token-array:last' => 0,
    'token-array:count' => 0,
    'token-array:reversed' => 0,
    'token-array:keys' => 0,
    'token-array:join' => 0,
    'token-array:value' => 0,
    'token-random:number' => 0,
    'token-random:hash' => 0,
    'token-facetapi_results:keys' => 0,
    'token-facetapi_results:page-number' => 0,
    'token-facetapi_results:page-limit' => 0,
    'token-facetapi_results:page-total' => 0,
    'token-facetapi_results:offset' => 0,
    'token-facetapi_results:start-count' => 0,
    'token-facetapi_results:end-count' => 0,
    'token-facetapi_results:result-count' => 0,
    'token-facetapi_active:active-value' => 0,
    'token-facetapi_active:active-value-raw' => 0,
    'token-facetapi_active:active-pos' => 0,
    'token-facetapi_active:facet-label' => 0,
    'token-facetapi_active:facet-name' => 0,
    'token-facetapi_facet:facet-label' => 0,
    'token-facetapi_facet:facet-name' => 0,
    'token-file-type:name' => 0,
    'token-file-type:machine-name' => 0,
    'token-file-type:count' => 0,
    'token-file-type:edit-url' => 0,
    'token-site:name' => 0,
    'token-site:slogan' => 0,
    'token-site:mail' => 0,
    'token-site:url' => 0,
    'token-site:url-brief' => 0,
    'token-site:login-url' => 0,
    'token-site:current-user' => 0,
    'token-site:current-date' => 0,
    'token-site:current-page' => 0,
    'token-date:short' => 0,
    'token-date:medium' => 0,
    'token-date:long' => 0,
    'token-date:custom' => 0,
    'token-date:since' => 0,
    'token-date:raw' => 0,
    'token-view:name' => 0,
    'token-view:description' => 0,
    'token-view:machine-name' => 0,
    'token-view:title' => 0,
    'token-view:url' => 0,
    'token-menu:name' => 0,
    'token-menu:machine-name' => 0,
    'token-menu:description' => 0,
    'token-menu:menu-link-count' => 0,
    'token-menu:edit-url' => 0,
    'token-search_api_server:id' => 0,
    'token-search_api_server:name' => 0,
    'token-search_api_server:description' => 0,
    'token-search_api_server:class' => 0,
    'token-search_api_server:enabled' => 0,
    'token-search_api_server:url' => 0,
    'token-search_api_server:original' => 0,
    'token-search_api_index:id' => 0,
    'token-search_api_index:name' => 0,
    'token-search_api_index:description' => 0,
    'token-search_api_index:server-entity' => 0,
    'token-search_api_index:enabled' => 0,
    'token-search_api_index:read-only' => 0,
    'token-search_api_index:url' => 0,
    'token-search_api_index:original' => 0,
    'token-rules_config:id' => 0,
    'token-rules_config:label' => 0,
    'token-rules_config:plugin' => 0,
    'token-rules_config:active' => 0,
    'token-rules_config:weight' => 0,
    'token-rules_config:status' => 0,
    'token-rules_config:dirty' => 0,
    'token-rules_config:module' => 0,
    'token-rules_config:access-exposed' => 0,
    'token-rules_config:original' => 0,
    'token-list<node>:0' => 0,
    'token-list<node>:1' => 0,
    'token-list<node>:2' => 0,
    'token-list<node>:3' => 0,
    'token-list<term>:0' => 0,
    'token-list<term>:1' => 0,
    'token-list<term>:2' => 0,
    'token-list<term>:3' => 0,
    'token-list<search_api_server>:0' => 0,
    'token-list<search_api_server>:1' => 0,
    'token-list<search_api_server>:2' => 0,
    'token-list<search_api_server>:3' => 0,
    'token-list<search_api_index>:0' => 0,
    'token-list<search_api_index>:1' => 0,
    'token-list<search_api_index>:2' => 0,
    'token-list<search_api_index>:3' => 0,
    'token-list<file>:0' => 0,
    'token-list<file>:1' => 0,
    'token-list<file>:2' => 0,
    'token-list<file>:3' => 0,
    'token-list<vocabulary>:0' => 0,
    'token-list<vocabulary>:1' => 0,
    'token-list<vocabulary>:2' => 0,
    'token-list<vocabulary>:3' => 0,
    'token-list<user>:0' => 0,
    'token-list<user>:1' => 0,
    'token-list<user>:2' => 0,
    'token-list<user>:3' => 0,
    'token-list<rules_config>:0' => 0,
    'token-list<rules_config>:1' => 0,
    'token-list<rules_config>:2' => 0,
    'token-list<rules_config>:3' => 0,
    'token-list<date>:0' => 0,
    'token-list<date>:1' => 0,
    'token-list<date>:2' => 0,
    'token-list<date>:3' => 0,
    'entity_field-node:title_field' => 'entity_field-node:title_field',
    'entity_field-node:field_ns_prod_entrpr_user' => 'entity_field-node:field_ns_prod_entrpr_user',
    'entity_field-node:field_ns_blog_description_long' => 'entity_field-node:field_ns_blog_description_long',
    'entity_field-node:field_ns_blog_description_short' => 'entity_field-node:field_ns_blog_description_short',
    'entity_field-node:field_ns_blog_image' => 'entity_field-node:field_ns_blog_image',
    'entity_field-node:field_ns_prod_entrpr_syndication' => 'entity_field-node:field_ns_prod_entrpr_syndication',
    'entity_field-node:field_ns_prod_entrpr_tag' => 'entity_field-node:field_ns_prod_entrpr_tag',
    'entity_field-node:field_ns_blog_post_blog' => 'entity_field-node:field_ns_blog_post_blog',
    'entity_field-node:field_ns_blog_post_body' => 'entity_field-node:field_ns_blog_post_body',
    'entity_field-node:field_ns_blog_post_media' => 'entity_field-node:field_ns_blog_post_media',
    'entity_field-node:field_ns_blog_post_lead' => 'entity_field-node:field_ns_blog_post_lead',
    'entity_field-node:field_ns_prod_entrpr_body' => 'entity_field-node:field_ns_prod_entrpr_body',
    'entity_field-node:field_ns_prod_entrpr_case_study' => 'entity_field-node:field_ns_prod_entrpr_case_study',
    'entity_field-node:field_ns_prod_entrpr_client' => 'entity_field-node:field_ns_prod_entrpr_client',
    'entity_field-node:field_ns_prod_entrpr_lead' => 'entity_field-node:field_ns_prod_entrpr_lead',
    'entity_field-node:field_ns_prod_entrpr_media' => 'entity_field-node:field_ns_prod_entrpr_media',
    'entity_field-node:field_ns_prod_entrpr_promo' => 'entity_field-node:field_ns_prod_entrpr_promo',
    'entity_field-node:field_ns_prod_entrpr_url' => 'entity_field-node:field_ns_prod_entrpr_url',
    'entity_field-node:field_ns_page_body' => 'entity_field-node:field_ns_page_body',
    'entity_field-node:field_ns_page_media' => 'entity_field-node:field_ns_page_media',
    'entity_field-node:field_ns_page_lead' => 'entity_field-node:field_ns_page_lead',
    'entity_field-node:field_ns_promo_description' => 'entity_field-node:field_ns_promo_description',
    'entity_field-node:field_ns_promo_link' => 'entity_field-node:field_ns_promo_link',
    'entity_field-node:field_ns_promo_media' => 'entity_field-node:field_ns_promo_media',
    'entity_field-node:field_ns_prod_entrpr_slideshow' => 'entity_field-node:field_ns_prod_entrpr_slideshow',
    'entity_field-node:field_ns_prod_entrpr_fact' => 'entity_field-node:field_ns_prod_entrpr_fact',
    'entity_field-taxonomy_term:description_field' => 'entity_field-taxonomy_term:description_field',
    'entity_field-taxonomy_term:name_field' => 'entity_field-taxonomy_term:name_field',
    'entity_field-taxonomy_term:field_ns_prod_entrpr_address' => 'entity_field-taxonomy_term:field_ns_prod_entrpr_address',
    'entity_field-taxonomy_term:field_ns_prod_entrpr_body' => 'entity_field-taxonomy_term:field_ns_prod_entrpr_body',
    'entity_field-taxonomy_term:field_ns_prod_entrpr_email' => 'entity_field-taxonomy_term:field_ns_prod_entrpr_email',
    'entity_field-taxonomy_term:field_ns_prod_entrpr_media' => 'entity_field-taxonomy_term:field_ns_prod_entrpr_media',
    'entity_field-taxonomy_term:field_ns_prod_entrpr_phone' => 'entity_field-taxonomy_term:field_ns_prod_entrpr_phone',
    'entity_field-taxonomy_term:field_ns_prod_entrpr_location' => 'entity_field-taxonomy_term:field_ns_prod_entrpr_location',
    'entity_field-file:field_ns_media_caption' => 'entity_field-file:field_ns_media_caption',
    'entity_field-file:field_ns_media_category' => 'entity_field-file:field_ns_media_category',
    'entity_field-file:field_ns_media_tag' => 'entity_field-file:field_ns_media_tag',
    'entity_field-user:field_ns_prod_entrpr_body' => 'entity_field-user:field_ns_prod_entrpr_body',
    'entity_field-user:field_ns_prod_entrpr_email' => 'entity_field-user:field_ns_prod_entrpr_email',
    'entity_field-user:field_ns_prod_entrpr_first_name' => 'entity_field-user:field_ns_prod_entrpr_first_name',
    'entity_field-user:field_ns_prod_entrpr_last_name' => 'entity_field-user:field_ns_prod_entrpr_last_name',
    'entity_field-user:field_ns_prod_entrpr_lead' => 'entity_field-user:field_ns_prod_entrpr_lead',
    'entity_field-user:field_ns_prod_entrpr_link' => 'entity_field-user:field_ns_prod_entrpr_link',
    'entity_field-user:field_ns_prod_entrpr_media' => 'entity_field-user:field_ns_prod_entrpr_media',
    'entity_field-user:field_ns_prod_entrpr_phone' => 'entity_field-user:field_ns_prod_entrpr_phone',
    'entity_field-user:field_ns_prod_entrpr_position' => 'entity_field-user:field_ns_prod_entrpr_position',
    'entity_field-user:field_ns_prod_entrpr_office' => 'entity_field-user:field_ns_prod_entrpr_office',
    'entity_field_extra-taxonomy_term:description' => 'entity_field_extra-taxonomy_term:description',
    'entity_field_extra-file:file' => 'entity_field_extra-file:file',
    'entity_field_extra-user:summary' => 'entity_field_extra-user:summary',
    'entity_form_field-node:title_field' => 'entity_form_field-node:title_field',
    'entity_form_field-node:field_ns_prod_entrpr_user' => 'entity_form_field-node:field_ns_prod_entrpr_user',
    'entity_form_field-node:field_ns_blog_description_long' => 'entity_form_field-node:field_ns_blog_description_long',
    'entity_form_field-node:field_ns_blog_description_short' => 'entity_form_field-node:field_ns_blog_description_short',
    'entity_form_field-node:field_ns_blog_image' => 'entity_form_field-node:field_ns_blog_image',
    'entity_form_field-node:field_ns_prod_entrpr_syndication' => 'entity_form_field-node:field_ns_prod_entrpr_syndication',
    'entity_form_field-node:field_ns_prod_entrpr_tag' => 'entity_form_field-node:field_ns_prod_entrpr_tag',
    'entity_form_field-node:field_ns_blog_post_blog' => 'entity_form_field-node:field_ns_blog_post_blog',
    'entity_form_field-node:field_ns_blog_post_body' => 'entity_form_field-node:field_ns_blog_post_body',
    'entity_form_field-node:field_ns_blog_post_media' => 'entity_form_field-node:field_ns_blog_post_media',
    'entity_form_field-node:field_ns_blog_post_lead' => 'entity_form_field-node:field_ns_blog_post_lead',
    'entity_form_field-node:field_ns_prod_entrpr_body' => 'entity_form_field-node:field_ns_prod_entrpr_body',
    'entity_form_field-node:field_ns_prod_entrpr_case_study' => 'entity_form_field-node:field_ns_prod_entrpr_case_study',
    'entity_form_field-node:field_ns_prod_entrpr_client' => 'entity_form_field-node:field_ns_prod_entrpr_client',
    'entity_form_field-node:field_ns_prod_entrpr_media' => 'entity_form_field-node:field_ns_prod_entrpr_media',
    'entity_form_field-node:field_ns_prod_entrpr_url' => 'entity_form_field-node:field_ns_prod_entrpr_url',
    'entity_form_field-node:field_ns_page_lead' => 'entity_form_field-node:field_ns_page_lead',
    'entity_form_field-node:field_ns_prod_entrpr_fact' => 'entity_form_field-node:field_ns_prod_entrpr_fact',
    'entity_form_field-taxonomy_term:description_field' => 'entity_form_field-taxonomy_term:description_field',
    'entity_form_field-taxonomy_term:name_field' => 'entity_form_field-taxonomy_term:name_field',
    'entity_form_field-taxonomy_term:field_ns_prod_entrpr_address' => 'entity_form_field-taxonomy_term:field_ns_prod_entrpr_address',
    'entity_form_field-taxonomy_term:field_ns_prod_entrpr_body' => 'entity_form_field-taxonomy_term:field_ns_prod_entrpr_body',
    'entity_form_field-taxonomy_term:field_ns_prod_entrpr_email' => 'entity_form_field-taxonomy_term:field_ns_prod_entrpr_email',
    'entity_form_field-taxonomy_term:field_ns_prod_entrpr_media' => 'entity_form_field-taxonomy_term:field_ns_prod_entrpr_media',
    'entity_form_field-taxonomy_term:field_ns_prod_entrpr_phone' => 'entity_form_field-taxonomy_term:field_ns_prod_entrpr_phone',
    'entity_form_field-taxonomy_term:field_ns_prod_entrpr_location' => 'entity_form_field-taxonomy_term:field_ns_prod_entrpr_location',
    'entity_form_field-user:field_ns_prod_entrpr_body' => 'entity_form_field-user:field_ns_prod_entrpr_body',
    'entity_form_field-user:field_ns_prod_entrpr_email' => 'entity_form_field-user:field_ns_prod_entrpr_email',
    'entity_form_field-user:field_ns_prod_entrpr_first_name' => 'entity_form_field-user:field_ns_prod_entrpr_first_name',
    'entity_form_field-user:field_ns_prod_entrpr_last_name' => 'entity_form_field-user:field_ns_prod_entrpr_last_name',
    'entity_form_field-user:field_ns_prod_entrpr_lead' => 'entity_form_field-user:field_ns_prod_entrpr_lead',
    'entity_form_field-user:field_ns_prod_entrpr_link' => 'entity_form_field-user:field_ns_prod_entrpr_link',
    'entity_form_field-user:field_ns_prod_entrpr_media' => 'entity_form_field-user:field_ns_prod_entrpr_media',
    'entity_form_field-user:field_ns_prod_entrpr_phone' => 'entity_form_field-user:field_ns_prod_entrpr_phone',
    'entity_form_field-user:field_ns_prod_entrpr_position' => 'entity_form_field-user:field_ns_prod_entrpr_position',
    'entity_form_field-user:field_ns_prod_entrpr_office' => 'entity_form_field-user:field_ns_prod_entrpr_office',
    'entity_form_field-node:field_ns_prod_entrpr_lead' => 0,
    'entity_form_field-node:field_ns_prod_entrpr_promo' => 0,
    'entity_form_field-node:field_ns_page_body' => 0,
    'entity_form_field-node:field_ns_page_media' => 0,
    'entity_form_field-node:field_ns_promo_description' => 0,
    'entity_form_field-node:field_ns_promo_link' => 0,
    'entity_form_field-node:field_ns_promo_media' => 0,
    'entity_form_field-node:field_ns_prod_entrpr_slideshow' => 0,
    'entity_form_field-file:field_ns_media_caption' => 0,
    'entity_form_field-file:field_ns_media_category' => 0,
    'entity_form_field-file:field_ns_media_tag' => 0,
    'block-diff-inline' => 0,
    'block-locale-language' => 0,
    'block-locale-language_content' => 0,
    'block-markdown-markdown_help' => 0,
    'block-menu-devel' => 0,
    'block-menu-features' => 0,
    'block-menu-menu-ns-prod-entrpr-footer-sec' => 0,
    'block-node-syndicate' => 0,
    'block-node-recent' => 0,
    'block-system-powered-by' => 0,
    'block-system-help' => 0,
    'block-system-navigation' => 0,
    'block-system-management' => 0,
    'block-system-user-menu' => 0,
    'block-system-main-menu' => 0,
    'block-user-login' => 0,
    'block-user-new' => 0,
    'block-user-online' => 0,
    'block-views-6791e618ffc95756b20d551d0e040603' => 0,
    'block-devel-execute_php' => 0,
    'block-devel-switch_user' => 0,
    'entity_view-node' => 0,
    'entity_view-taxonomy_term' => 0,
    'entity_view-search_api_server' => 0,
    'entity_view-search_api_index' => 0,
    'entity_view-file' => 0,
    'entity_view-user' => 0,
    'entity_view-rules_config' => 0,
    'menu_tree-_active' => 'menu_tree-_active',
    'menu_tree-main-menu' => 'menu_tree-main-menu',
    'menu_tree-management' => 'menu_tree-management',
    'menu_tree-navigation' => 'menu_tree-navigation',
    'menu_tree-user-menu' => 'menu_tree-user-menu',
    'menu_tree-devel' => 0,
    'menu_tree-features' => 0,
    'menu_tree-menu-ns-prod-entrpr-footer-sec' => 0,
    'openlayers_map-ns_prod_enterpise_office_offices' => 'openlayers_map-ns_prod_enterpise_office_offices',
    'openlayers_map-geofield_widget_map' => 0,
    'openlayers_map-geofield_formatter_map' => 0,
    'openlayers_map-ns_prod_enterprise_office_map' => 0,
    'openlayers_map-default' => 0,
    'openlayers_map-example_google' => 0,
    'openlayers_map-example_geojson' => 0,
    'openlayers_map-example_virtual_earth' => 0,
    'openlayers_map-example_osm' => 0,
    'panels_mini-ns_prod_entrpr_media' => 'panels_mini-ns_prod_entrpr_media',
    'panels_mini-ns_prod_entrpr_media_huge' => 'panels_mini-ns_prod_entrpr_media_huge',
    'panels_mini-ns_prod_entrpr_media_large' => 'panels_mini-ns_prod_entrpr_media_large',
    'panels_mini-ns_prod_entrpr_promo_huge' => 'panels_mini-ns_prod_entrpr_promo_huge',
    'panels_mini-ns_prod_entrpr_promo_large' => 'panels_mini-ns_prod_entrpr_promo_large',
    'panels_mini-ns_prod_entrpr_slide_twocol' => 'panels_mini-ns_prod_entrpr_slide_twocol',
    'panels_mini-ns_prod_entrpr_blog_info' => 'panels_mini-ns_prod_entrpr_blog_info',
    'panels_mini-ns_prod_entrpr_office' => 'panels_mini-ns_prod_entrpr_office',
    'panels_mini-ns_prod_entrpr_user' => 'panels_mini-ns_prod_entrpr_user',
    'panels_mini-ns_prod_entrpr_user_author' => 'panels_mini-ns_prod_entrpr_user_author',
    'panels_mini-ns_prod_entrpr_promo' => 0,
    'panels_mini-ns_prod_enterprise_teaser' => 0,
    'views_panes-ns_prod_enterprise_content-panel_pane_1' => 'views_panes-ns_prod_enterprise_content-panel_pane_1',
    'views_panes-ns_prod_enterprise_content-panel_pane_3' => 'views_panes-ns_prod_enterprise_content-panel_pane_3',
    'views_panes-ns_prod_enterprise_content-panel_pane_4' => 'views_panes-ns_prod_enterprise_content-panel_pane_4',
    'views_panes-ns_prod_enterprise_content-panel_pane_2' => 'views_panes-ns_prod_enterprise_content-panel_pane_2',
    'views_panes-ns_prod_enterprise_footer_content-panel_pane_1' => 'views_panes-ns_prod_enterprise_footer_content-panel_pane_1',
    'views_panes-ns_prod_enterprise_tag_content-panel_pane_1' => 'views_panes-ns_prod_enterprise_tag_content-panel_pane_1',
    'views_panes-ns_prod_enterprise_tags-panel_pane_1' => 'views_panes-ns_prod_enterprise_tags-panel_pane_1',
    'views_panes-ns_prod_enterprise_blog-panel_pane_3' => 'views_panes-ns_prod_enterprise_blog-panel_pane_3',
    'views_panes-ns_prod_enterprise_blog-panel_pane_4' => 'views_panes-ns_prod_enterprise_blog-panel_pane_4',
    'views_panes-ns_prod_enterprise_blog-panel_pane_1' => 'views_panes-ns_prod_enterprise_blog-panel_pane_1',
    'views_panes-ns_prod_enterprise_blog_posts-panel_pane_3' => 'views_panes-ns_prod_enterprise_blog_posts-panel_pane_3',
    'views_panes-ns_prod_enterprise_blog_posts-panel_pane_4' => 'views_panes-ns_prod_enterprise_blog_posts-panel_pane_4',
    'views_panes-ns_prod_enterprise_blog_posts-panel_pane_1' => 'views_panes-ns_prod_enterprise_blog_posts-panel_pane_1',
    'views_panes-ns_prod_enterprise_blog_posts-panel_pane_2' => 'views_panes-ns_prod_enterprise_blog_posts-panel_pane_2',
    'views_panes-ns_prod_enterprise_client_case_categories-panel_pane_1' => 'views_panes-ns_prod_enterprise_client_case_categories-panel_pane_1',
    'views_panes-ns_prod_enterprise_client_cases-panel_pane_3' => 'views_panes-ns_prod_enterprise_client_cases-panel_pane_3',
    'views_panes-ns_prod_enterprise_client_cases-panel_pane_2' => 'views_panes-ns_prod_enterprise_client_cases-panel_pane_2',
    'views_panes-ns_prod_enterprise_client_cases-panel_pane_1' => 'views_panes-ns_prod_enterprise_client_cases-panel_pane_1',
    'views_panes-ns_prod_enterprise_client_cases-panel_pane_4' => 'views_panes-ns_prod_enterprise_client_cases-panel_pane_4',
    'views_panes-ns_prod_enterprise_office-panel_pane_1' => 'views_panes-ns_prod_enterprise_office-panel_pane_1',
    'views_panes-ns_prod_enterprise_office-panel_pane_2' => 'views_panes-ns_prod_enterprise_office-panel_pane_2',
    'views_panes-ns_prod_enterprise_office-panel_pane_3' => 'views_panes-ns_prod_enterprise_office-panel_pane_3',
    'views_panes-ns_prod_enterprise_search-panel_pane_1' => 'views_panes-ns_prod_enterprise_search-panel_pane_1',
    'views_panes-ns_prod_enterprise_users-panel_pane_1' => 'views_panes-ns_prod_enterprise_users-panel_pane_1',
    'views_panes-ns_prod_enterprise_users-panel_pane_2' => 'views_panes-ns_prod_enterprise_users-panel_pane_2',
    'views_panes-ns_prod_enterprise_users-panel_pane_3' => 'views_panes-ns_prod_enterprise_users-panel_pane_3',
    'views_panes-ns_prod_enterprise_users-panel_pane_4' => 'views_panes-ns_prod_enterprise_users-panel_pane_4',
    'views-media_default' => 0,
    'views-menu_existing_content_selection' => 0,
    'views-ns_blog_post_reference_search' => 0,
    'views-ns_blog_reference_search' => 0,
    'views-ns_media_browser' => 0,
    'views-ns_media_contributor_search' => 0,
    'views-ns_page_reference_search' => 0,
    'views-ns_prod_blog_rss' => 0,
    'views-ns_prod_enterprise_blog' => 0,
    'views-ns_prod_enterprise_blog_posts' => 0,
    'views-ns_prod_enterprise_client_cases' => 0,
    'views-ns_prod_enterprise_client_case_categories' => 0,
    'views-ns_prod_enterprise_content' => 0,
    'views-ns_prod_enterprise_content_admin' => 0,
    'views-ns_prod_enterprise_footer_content' => 0,
    'views-ns_prod_enterprise_office' => 0,
    'views-ns_prod_enterprise_office_map' => 0,
    'views-ns_prod_enterprise_ref_search' => 0,
    'views-ns_prod_enterprise_search' => 0,
    'views-ns_prod_enterprise_tags' => 0,
    'views-ns_prod_enterprise_tag_content' => 0,
    'views-ns_prod_enterprise_taxonomy_admin' => 0,
    'views-ns_prod_enterprise_users' => 0,
    'page_title-page_title' => 'page_title-page_title',
    'node_title-node_title' => 'node_title-node_title',
    'user_profile-user_profile' => 0,
    'user_signature-user_signature' => 0,
    'user_picture-user_picture' => 0,
    'node_form_publishing-node_form_publishing' => 0,
    'node_form_menu-node_form_menu' => 0,
    'node_form_log-node_form_log' => 0,
    'node_form_buttons-node_form_buttons' => 0,
    'node_form_author-node_form_author' => 0,
    'node_form_title-node_form_title' => 0,
    'vocabulary_terms-vocabulary_terms' => 0,
    'page_tabs-page_tabs' => 0,
    'page_actions-page_actions' => 0,
    'page_breadcrumb-page_breadcrumb' => 0,
    'page_slogan-page_slogan' => 0,
    'page_feed_icons-page_feed_icons' => 0,
    'page_messages-page_messages' => 0,
    'page_help-page_help' => 0,
    'page_primary_links-page_primary_links' => 0,
    'page_site_name-page_site_name' => 0,
    'page_secondary_links-page_secondary_links' => 0,
    'page_logo-page_logo' => 0,
    'node-node' => 0,
    'term_list-term_list' => 0,
    'term_description-term_description' => 0,
    'node_links-node_links' => 0,
    'node_created-node_created' => 0,
    'node_content-node_content' => 0,
    'node_body-node_body' => 0,
    'node_author-node_author' => 0,
    'node_attachments-node_attachments' => 0,
    'node_type_desc-node_type_desc' => 0,
    'node_terms-node_terms' => 0,
    'node_updated-node_updated' => 0,
    'form-form' => 0,
    'file_content-file_content' => 0,
    'file_display-file_display' => 0,
    'today_date-today_date' => 0,
    'node_date_custom-node_date_custom' => 0,
    'page_title_markup-page_title_markup' => 0,
    'term_title_markup-term_title_markup' => 0,
    'translation_status-translation_status' => 0,
    'pane_header-pane_header' => 0,
    'pane_messages-pane_messages' => 0,
    'pane_navigation-pane_navigation' => 0,
    'page_content-page_content' => 0,
    'views_empty-views_empty' => 0,
    'views_view-views_view' => 0,
    'views_footer-views_footer' => 0,
    'views_header-views_header' => 0,
    'views_row-views_row' => 0,
    'views_attachments-views_attachments' => 0,
    'views_feed-views_feed' => 0,
    'views_pager-views_pager' => 0,
    'views_exposed-views_exposed' => 0,
    'panelizer_form_default-panelizer_form_default' => 0,
  );
  $export['panelizer_node:ns_prod_enterprise_section_allowed_types'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_node:ns_prod_enterprise_section_default';
  $strongarm->value = array(
    'entity_field' => 'entity_field',
    'entity_field_extra' => 'entity_field_extra',
    'entity_form_field' => 'entity_form_field',
    'panels_mini' => 'panels_mini',
    'views_panes' => 'views_panes',
    'context_admin' => 0,
    'custom' => 0,
    'token' => 0,
    'block' => 0,
    'entity_view' => 0,
    'menu_tree' => 0,
    'openlayers_map' => 0,
    'views' => 0,
    'other' => 0,
  );
  $export['panelizer_node:ns_prod_enterprise_section_default'] = $strongarm;

  return $export;
}
