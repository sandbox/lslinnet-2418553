<?php
/**
 * @file
 * ns_prod_enterprise.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function ns_prod_enterprise_user_default_permissions() {
  $permissions = array();

  // Exported permission: administer menu.
  $permissions['administer menu'] = array(
    'name' => 'administer menu',
    'roles' => array(
      0 => 'administrator',
      1 => 'chief editor',
      2 => 'editor',
      3 => 'super user',
    ),
    'module' => 'menu',
  );

  // Exported permission: administer panelizer node ns_prod_enterprise_section content.
  $permissions['administer panelizer node ns_prod_enterprise_section content'] = array(
    'name' => 'administer panelizer node ns_prod_enterprise_section content',
    'roles' => array(),
    'module' => 'panelizer',
  );

  // Exported permission: administer panelizer node ns_prod_enterprise_section context.
  $permissions['administer panelizer node ns_prod_enterprise_section context'] = array(
    'name' => 'administer panelizer node ns_prod_enterprise_section context',
    'roles' => array(),
    'module' => 'panelizer',
  );

  // Exported permission: administer panelizer node ns_prod_enterprise_section layout.
  $permissions['administer panelizer node ns_prod_enterprise_section layout'] = array(
    'name' => 'administer panelizer node ns_prod_enterprise_section layout',
    'roles' => array(),
    'module' => 'panelizer',
  );

  // Exported permission: administer panelizer node ns_prod_enterprise_section settings.
  $permissions['administer panelizer node ns_prod_enterprise_section settings'] = array(
    'name' => 'administer panelizer node ns_prod_enterprise_section settings',
    'roles' => array(),
    'module' => 'panelizer',
  );

  // Exported permission: administer panels styles.
  $permissions['administer panels styles'] = array(
    'name' => 'administer panels styles',
    'roles' => array(
      0 => 'chief editor',
    ),
    'module' => 'panels',
  );

  // Exported permission: change layouts in place editing.
  $permissions['change layouts in place editing'] = array(
    'name' => 'change layouts in place editing',
    'roles' => array(
      0 => 'chief editor',
    ),
    'module' => 'panels',
  );

  // Exported permission: create ns_prod_enterprise_section content.
  $permissions['create ns_prod_enterprise_section content'] = array(
    'name' => 'create ns_prod_enterprise_section content',
    'roles' => array(
      0 => 'administrator',
      1 => 'chief editor',
      2 => 'editor',
      3 => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any ns_prod_enterprise_section content.
  $permissions['delete any ns_prod_enterprise_section content'] = array(
    'name' => 'delete any ns_prod_enterprise_section content',
    'roles' => array(
      0 => 'administrator',
      1 => 'chief editor',
      2 => 'editor',
      3 => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own ns_prod_enterprise_section content.
  $permissions['delete own ns_prod_enterprise_section content'] = array(
    'name' => 'delete own ns_prod_enterprise_section content',
    'roles' => array(
      0 => 'administrator',
      1 => 'chief editor',
      2 => 'editor',
      3 => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any ns_prod_enterprise_section content.
  $permissions['edit any ns_prod_enterprise_section content'] = array(
    'name' => 'edit any ns_prod_enterprise_section content',
    'roles' => array(
      0 => 'administrator',
      1 => 'chief editor',
      2 => 'editor',
      3 => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own ns_prod_enterprise_section content.
  $permissions['edit own ns_prod_enterprise_section content'] = array(
    'name' => 'edit own ns_prod_enterprise_section content',
    'roles' => array(
      0 => 'administrator',
      1 => 'chief editor',
      2 => 'editor',
      3 => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: use panels in place editing.
  $permissions['use panels in place editing'] = array(
    'name' => 'use panels in place editing',
    'roles' => array(
      0 => 'chief editor',
      1 => 'super user',
    ),
    'module' => 'panels',
  );

  return $permissions;
}
